#include "TM1637Display.h"
#include <DS1302.h>

// relays
#define relay1_pin 2

// buttons
#define LONG_BUTTON_DELAY 500
#define BUTTON_1 A0
#define BUTTON_2 A1

// LCD
#define LCD_CLK 11
#define LCD_DIO 12

// clock
#define CLOCK_CLK 8
#define CLOCK_DATA 7
#define CLOCK_RST 6

TM1637Display display(LCD_CLK, LCD_DIO);
DS1302 rtc(CLOCK_RST, CLOCK_DATA, CLOCK_CLK);

void setup() {
  Serial.begin(9600);
  startlog();
  Serial.println("start...");
  endlog();
 
  //relay
  pinMode(relay1_pin, OUTPUT);
  digitalWrite(relay1_pin, HIGH); //inverted output

  // buttons
  pinMode(BUTTON_1, INPUT); 
  digitalWrite(BUTTON_1, HIGH);
  pinMode(BUTTON_2, INPUT);
  digitalWrite(BUTTON_2, HIGH);   
  
  // lcd
  display.setBrightness(0x0f);  
  uint8_t data[] = { 0xff, 0xff, 0xff, 0xff };
  display.setSegments(data);  
  
  // clock
  rtc.halt(false);
  rtc.writeProtect(false);
  
  // The following lines can be commented out to use the values already stored in the DS1302
  //rtc.setTime(12, 59, 0);     // Set the time to 12:00:00 (24hr format)
  
  delay(500);
}

void startlog() {
  Serial.print("#S|LOG|[");
}

void endlog() {
  Serial.println("]#");
}

bool button1_down = 0;
int button1_down_time;
bool button2_down = 0;
int button2_down_time;

void scan_key(int pin, bool &button_down, int &button_down_time, void (*callback0)(void), void (*callback1)(void), void (*callback2)(void)) {
  int result = analogRead(pin);
  bool pressed = result < 200; 
  if (pressed && !button_down) {
    button_down = 1;
    button_down_time = millis();
    callback0();
  }
  if (pressed && button_down) {
    callback2();
  }
  if (!pressed && button_down) {
    button_down = 0;
    int interval = millis() - button_down_time;
    bool is_long = interval > LONG_BUTTON_DELAY;
    callback1();
  }
}

bool tuning = 0;
int tuningStart;
int tuningHour, tuningMin;
int oldPressTime1, oldPressTime2;

void on_key_down(int &oldPressTime) {
  tuning = 1;  
  Time time = rtc.getTime();
  tuningHour = time.hour;
  tuningMin = time.min;
  oldPressTime = millis();
  tuningStart = millis();
}

void on_key1_down() {
  on_key_down(oldPressTime1);
}

void on_key2_down() {
  on_key_down(oldPressTime2);
}

void on_key_up() {
  tuning = 0;
  rtc.setTime(tuningHour, tuningMin, 0);
  
  startlog();
  Serial.print("operation: new time has been set");
  endlog();
}

void on_key_pressed(int &oldPressTime, int &tuningValue, int maxValue) {
  int interval = 600;
  if ((millis() - tuningStart) > 3000)
    interval = 300;
  
  int deltaTime = millis() - oldPressTime;
  int n = deltaTime / interval;
  tuningValue = (tuningValue + n) % maxValue;
  oldPressTime += n * interval;
}

void on_key1_pressed() {  
  on_key_pressed(oldPressTime1, tuningHour, 24);
}

void on_key2_pressed() {
  on_key_pressed(oldPressTime2, tuningMin, 60);
}

void scan_keys() {
  scan_key(BUTTON_1, button1_down, button1_down_time, &on_key1_down, &on_key_up, &on_key1_pressed);
  scan_key(BUTTON_2, button2_down, button2_down_time, &on_key2_down, &on_key_up, &on_key2_pressed);  
};

void update_time_on_lcd(int hr, int mn) {
  uint8_t data[4];
  data[0] = display.encodeDigit(hr / 10);
  data[1] = display.encodeDigit(hr % 10);
  data[2] = display.encodeDigit(mn / 10);
  data[3] = display.encodeDigit(mn % 10);
  display.setSegments(data);
  delay(50);
}

extern void process_relays();
extern void update_time_on_lcd();
extern void print_relays();

bool rl1_on;

Time currentTime;
int lastShowTime = 0;
void loop() {  
  delay(50);
  
  scan_keys();  
  process_relays();

  if ((millis() - lastShowTime) > 200) {
    update_time_on_lcd();
    print_status();
    
    lastShowTime = millis();
  }  
}

void process_relays() {
  rl1_on = (currentTime.hour >= 8 && currentTime.hour < 22);
  digitalWrite(relay1_pin, rl1_on ? LOW : HIGH); //inverted output
}

void update_time_on_lcd() {
  if (tuning) {
    update_time_on_lcd(tuningHour, tuningMin);
  } else {
    currentTime = rtc.getTime();
    update_time_on_lcd(currentTime.hour, currentTime.min);
  }
}

void print_status() {
  startlog();
  Serial.print("status: rl1 ");
  Serial.print(rl1_on ? "1" : "0");
  Serial.print("\ttime ");
  Serial.print(currentTime.hour);
  Serial.print("h ");
  Serial.print(currentTime.min);
  Serial.print("m");
  Serial.println();
  endlog();
}

