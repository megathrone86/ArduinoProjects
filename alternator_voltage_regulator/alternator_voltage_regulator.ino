//  DIGITAL    ANALOG
//  +-\/-+     +-\/-+
// 5|o   |+   0|o   |
// 3|    |2   3|    |1
// 4|    |1   2|    |
// -|    |0    |    |
//  +----+     +----+

#define analogInPin 2
#define voltageDividerRatio 4 //коэффициент делителя напряжения (если максимально нужно измерять 20 вольт, а на вход можно подавать максимум 5 вольт, то надо делить на 4)
#define sensorValuesCount 3

//в идеальных условиях (делитель 1:4, значение 1024 на входе соответствует 5в)
//максимальное напряжение должно быть 14.2в, т.е. после преобразования будет 14.2*1024 / 4 / 5 = 727
//точные цифры подбираются под каждую схему индивидуально
#define maxVoltage1 727
#define maxVoltage2 727

//испытания показали, что после прогрева мотоцикла напряжение почему-то падает, поэтому придется использовать два значения,
//иначе либо на прогреве будет перезаряд, либо после прогрева будет недозаряд
#define voltageSwitchTimeMillis 75*1000

byte currentRelayMode;
#define voltageHigh LOW
#define voltageLow HIGH

void relayMode(byte mode) {
  if (currentRelayMode == mode)
    return;
  currentRelayMode = mode;

  //+5 - открывает реле
  //0 - закрывает

  for (byte i = 0; i < 2; i++) {
    digitalWrite(i, mode);
  }
}

void setup() {
  for (uint8_t i = 0; i < 2; i++) {
    pinMode(i, OUTPUT);
  }

  for (uint8_t i = 0; i < 3; i++) {
    relayMode(HIGH);
    delay(1000);
    relayMode(LOW);
    delay(300);
  }
}

#define NEVER -1
unsigned long startTime = NEVER;
unsigned long lastHighVoltageTime = 0;

void loop() {
  //измерим напряжение
  word sensorValuesSum = 0;
  for (byte i = 0; i < sensorValuesCount; i++) {
    sensorValuesSum += analogRead(analogInPin);
  }

  //определим текущую норму напряжения (первые N секунд maxVoltage1, потом maxVoltage2)
  bool firstTimePeriod = startTime == NEVER || (millis() - startTime) < voltageSwitchTimeMillis;
  uint8_t currentMaxVoltage = firstTimePeriod ? maxVoltage1 : maxVoltage2;

  //если оно выше нормы
  if (sensorValuesSum / sensorValuesCount > currentMaxVoltage)
  {
    //то переключаем реле в режим voltage high
    relayMode(voltageHigh);
    lastHighVoltageTime = millis();
    if (startTime == NEVER)
      startTime = millis();
  } else {
    //то переключаем реле в режим voltage low
    relayMode(voltageLow);
    if (millis() - lastHighVoltageTime > voltageSwitchTimeMillis)
      startTime == NEVER;
  }

  //On-off Time ≤10 mS
  delay(12);
}
