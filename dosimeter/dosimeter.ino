/*Serial Communication for Radiation Detector Arduino Compatible DIY Kit ver 2.01 or higher
  http://radiohobbystore.com/radiation-detector-geiger-counter-diy-kit-second-edition.html
  Allow to connect the kit to computer and use the kit with Radiation Logger PC software
  http://radiohobbystore.com/radiation-logger/
  This Arduino sketch written by Alex Boguslavsky RH Electronics; mail: support@radiohobbystore.com
  CPM counting algorithm is very simple, it just collect GM Tube events during presettable log period.
  For radiation monitoring station it's recommended to use 30-60 seconds logging period. Feel free to modify
  or add functions to this sketch. This Arduino software is an example only for education purpose without any
  warranty for precision radiation measurements. You are fully responsible for your safety in high
  radiation area!!
  --------------------------------------------------------------------------------------
  WHAT IS CPM?
  CPM (or counts per minute) is events quantity from Geiger Tube you get during one minute. Usually it used to
  calculate a radiation level. Different GM Tubes has different quantity of CPM for background. Some tubes can produce
  about 10-50 CPM for normal background, other GM Tube models produce 50-100 CPM or 0-5 CPM for same radiation level.
  Please refer your GM Tube datasheet for more information. Just for reference here, SBM-20 can generate
  about 10-50 CPM for normal background.
  --------------------------------------------------------------------------------------
  HOW TO CONNECT GEIGER KIT?
  The kit 3 wires that should be connected to Arduino UNO board: 5V, GND and INT. PullUp resistor is included on
  kit PCB. Connect INT wire to Digital Pin#2 (INT0), 5V to 5V, GND to GND. Then connect the Arduino with
  USB cable to the computer and upload this sketch.
*/


#include <SPI.h>

#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2); // Устанавливаем дисплей
unsigned long displayMillis;
byte displayShift = 0;

#define GEIGER_INTERRUPT_PIN 0 //соответствует цифровому выводу 2

#define SHORT_PERIOD 30*1000ul //короткий период для быстрых измерений
#define LONG_PERIOD 10*60*1000ul //длинный период для точных измерений
#define MAX_DISPLAY_SHIFT 1 //максимальное расстояние прокрутки дисплея

unsigned long shortCounts = 0;     //variable for GM Tube events
unsigned long longCounts = 0;     //variable for GM Tube events
unsigned long lastCpm = 0;
unsigned long shortPeriodMillis;  //variable for time measurement
unsigned long longPeriodMillis;  //variable for time measurement

bool blinkOn = false;
unsigned long blinkMillis = 0;

void tube_impulse() {       //subprocedure for capturing events from Geiger Kit
  shortCounts++;
  longCounts++;
}

void setup() {
  attachInterrupt(GEIGER_INTERRUPT_PIN, tube_impulse, FALLING);
  pinMode(LED_BUILTIN, OUTPUT);

  lcd.init();
  lcd.backlight();

  lcd.print("test");
  delay(500);

  startShortPeriod();
  startLongPeriod();
}

void startShortPeriod() {
  shortCounts = 0;
  shortPeriodMillis = millis();
}
void startLongPeriod() {
  lastCpm = longCounts * 60000 / (millis() - longPeriodMillis);
  longCounts = 0;
  longPeriodMillis = millis();
}

void loop() {
  unsigned long currentMillis = millis();

  //periods
  if ((currentMillis - shortPeriodMillis) > SHORT_PERIOD) {
    startShortPeriod();
  }
  if ((currentMillis - longPeriodMillis) > LONG_PERIOD) {
    startLongPeriod();
  }

  //blink
  if (currentMillis - blinkMillis > 800) {
    blinkOn = !blinkOn;
    digitalWrite(LED_BUILTIN, blinkOn);
    blinkMillis = currentMillis;
  }

  //display
  if (currentMillis - displayMillis > 1000) {
    if (displayShift > MAX_DISPLAY_SHIFT) {
      displayShift = 0;
    }

    displayMillis = currentMillis;
    updateDisplay(currentMillis);

    displayShift++;
  }
}

void updateDisplay(unsigned long currentMillis) {
  lcd.clear();
  char line1[20], line2[30];
  line1[0] = 0;
  line2[0] = 0;

  {
    unsigned long shortSeconds = ((currentMillis - shortPeriodMillis) / 1000);
    unsigned long period = (SHORT_PERIOD / 1000ul);
    unsigned long cpm = 0;
    if (shortSeconds > 0) {
      cpm = (shortCounts * 60l) / shortSeconds;
    }
    sprintf(line1, "%02d/%02d    cpm:%d", (word)shortSeconds, (word)period, (word)cpm);
  }

  {
    unsigned long longSeconds = ((currentMillis - longPeriodMillis) / 1000ul);
    unsigned long period = (LONG_PERIOD / 1000ul);
    unsigned long cpm = 0;
    if (longSeconds > 0) {
      cpm = longCounts * 60l / longSeconds;
    }
    if (displayShift == 0 || lastCpm <= 0) {
      sprintf(line2, "%03d/%03d  cpm:%d", (word)longSeconds, (word)period, (word)cpm);
    } else {
      sprintf(line2, "%03d/%03d prev:%d", (word)longSeconds, (word)period, (word)lastCpm);
    }
  }

  lcd.print(line1);
  lcd.setCursor(0, 1);
  lcd.print(line2);
}
