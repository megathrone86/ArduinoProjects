#include "OneWire.h"
#include "EEPROM.h"
#include "DallasTemperature.h"

//настройки датчиков температуры
#define dallas_temp_precision 9
#define dallas_gnd 2
#define dallas_pwr 3
#define dallas_sgn 4

//настройки рере
#define relay_pin 8

//настройки сирены
#define alarm_gnd 12
#define alarm_pwr 13
#define alarm_repeats_hot 4
#define alarm_repeats_cold 2
#define alarm_off_flag 0
#define alarm_hot_flag 1
#define alarm_cold_flag 2

//настройки отслеживания температуры
  //температура уставки (в градусах)
#define desiredtemp 37.9f
  //верхняя граница при которой включать сирену (в градусах)
#define panic_high (desiredtemp + 3.0f)
  //нижняя граница при которой включать сирену (в градуса)
#define panic_low (desiredtemp - 3.0f)
  //задержка после включения/выключения тэнов (чтобы не щелкать туда-сюда из-за инерции)
#define freeze_time_after_heat_change 4000
  //минимальное значеие тепловой инерции (в градусах)
#define min_temperature_delay 1.0f
  //максимальное значение тепловой инерции, нужно чтобы отфильтровать грубые ошибки алгоритма (в градусах)
#define max_temperature_delay 8.0f
  //задержка, чтобы сирена не начинала выть как только мы воткнули инкубатор в розетку (в мс)
#define low_panic_delay 5*60*1000 

//для работы с датчиками
OneWire oneWire(dallas_sgn);
DallasTemperature sensors(&oneWire);

//блок для вычисления задержки системы
float temperature_delay;
float last_off_temp;
float max_temp;

//блок для сохранения данных
byte* temperature_delay_ptr = (byte*)&temperature_delay;
int temperature_delay_last_write_time;
#define temperature_delay_write_interval 60*1000*30

//основные показатели
float humidity;
float temp_dallas1;
float temp_dallas2;

//0 - off
//1 - on
bool flag_heat;

uint8_t flag_panic;

void setup() {
  Serial.begin(9600);
  writelog("start...");
 
  //dallas sensors
  pinMode(dallas_gnd, OUTPUT);
  pinMode(dallas_pwr, OUTPUT);
  digitalWrite(dallas_gnd, LOW);
  digitalWrite(dallas_pwr, HIGH);  
  sensors.begin();    
  sensors.requestTemperatures();
  delay(2000);
  
  //relay
  pinMode(relay_pin, OUTPUT);
  
  //alarm
  pinMode(alarm_gnd, OUTPUT);
  pinMode(alarm_pwr, OUTPUT);
  digitalWrite(alarm_gnd, LOW);
  do_alarm(alarm_cold_flag);
  delay(500);
  do_alarm(alarm_hot_flag);  
  
  //log header
  writelog("\ttemp1\ttemp2\theat\t\tpanic\t\ttemp_delay\tdht hum");
  
  //logic init
  flag_heat = 0;
  
  for (int i=0; i<4;i++) {
    temperature_delay_ptr[i] = EEPROM.read(i);
  }  
  if (temperature_delay < min_temperature_delay || temperature_delay > max_temperature_delay || isnan(temperature_delay)) {
    Serial.print("loading failed. temperature_delay = ");
    Serial.print(temperature_delay);
    Serial.println();
    
    temperature_delay = min_temperature_delay;
  }
}

void do_alarm(byte mode) {
  int repeats = 0; 
  if (mode == alarm_hot_flag)
    repeats = alarm_repeats_hot;  
  if (mode == alarm_cold_flag)
    repeats = alarm_repeats_cold;
  
  for (int i=0;i<repeats;i++) {
    do_alarm();
    delay(200);   
  }
}

void do_alarm() {
  int hi_delay = 2;
  int low_delay = 10;
  int repeats = 4;
  
  for (int i=0;i<repeats;i++) {
    digitalWrite(alarm_pwr, HIGH);  
    delay(hi_delay);
    digitalWrite(alarm_pwr, LOW);  
    delay(low_delay);
  }
}

void readtemp() {
  sensors.requestTemperatures();

  float td1 = sensors.getTempCByIndex(0);
  float td2 = sensors.getTempCByIndex(1);
  float adelta = fabs(td1 - td2);
  if (adelta < 15)
  {  
    temp_dallas1 = td1;
    temp_dallas2 = td2;
  } else
    writelog("adelta too big! sensors are ignored");
}

bool updateHeat(bool newHeat) {
  if (flag_heat != newHeat)
  {
    flag_heat = newHeat;
    return 1;    
  } else
    return 0;  
}

void startlog() {
  Serial.print("#S|LOG|[");
}

void writelog(char *c) {
  startlog();
  Serial.print(c);
  endlog();
}

void endlog() {
  Serial.println("]#");
}

void mainlogic(float temp, bool &heatChanged) {
  if (temp >= desiredtemp - temperature_delay && flag_panic != alarm_cold_flag)
  {    
    //в данный момент происходит отключение    
    if (flag_heat)
    {
      //запомним при какой температуре оно произошло
      last_off_temp = temp;
      //и проинициализируем переменную для отслеживания роста температуры
      max_temp = temp;
      //отключим обогреватель
      heatChanged = updateHeat(0);
    }
  } 
  else
  {
    //в данный момент происходит включение
    heatChanged = updateHeat(1);
  }
  
  //если в данный момент обогрев отключен, то мы должны отследить когда прекратится рост температуры
  if (!flag_heat && last_off_temp > 0)
  {
    if (temp > max_temp)
      max_temp = temp;
    if (temp < max_temp)
    {
      //мы отслеживали рост температуры, и в данный момент он прекратился. подсчитаем задержку.    
      temperature_delay = (max_temp - last_off_temp)*0.9;
      if (temperature_delay > max_temperature_delay)
        temperature_delay = max_temperature_delay;
      
      if ((millis() - temperature_delay_last_write_time) > temperature_delay_write_interval) {
        for (int i=0; i<4;i++) {
          EEPROM.write(i, temperature_delay_ptr[i]);
        }
        writelog("temperature_delay saved");
        temperature_delay_last_write_time = millis();
      }
      
      //и сбросим переменную для отслеживания роста температуры
      last_off_temp = 0;
    }
  }
    
  //отслеживание режима паники
  {
    flag_panic = alarm_off_flag;
    if (temp > panic_high)
      flag_panic = alarm_hot_flag;
    if ((millis() > low_panic_delay) && (temp < panic_low))
        flag_panic = alarm_cold_flag;        
  }
  
  if (heatChanged)
    setheat();  
    
  if (flag_panic != alarm_off_flag) {
    do_alarm(flag_panic);
    delay(500);
  }
}

void setheat() {
  if (flag_heat) {
    writelog("heat: opening");
    digitalWrite(relay_pin, HIGH);
  }
  else {
    writelog("heat: closing");
    digitalWrite(relay_pin, LOW);
  }
  
  delay(freeze_time_after_heat_change);
}

void printdata() {
  startlog();
    
  Serial.print(millis()/1000);
  Serial.print("\t");
  Serial.print(temp_dallas1);
  Serial.print("\t");
  Serial.print(temp_dallas2);
  Serial.print("\t");

  if (flag_heat)
    Serial.print("heat on ");
  else
    Serial.print("heat off");
    
  Serial.print("\t");
  
  if (flag_panic == alarm_off_flag)
    Serial.print("         ");
  if (flag_panic == alarm_hot_flag)
    Serial.print("panic HI ");
  if (flag_panic == alarm_cold_flag)
    Serial.print("panic LOW");

  Serial.print("\t");
  Serial.print(temperature_delay);
  Serial.print("\t\t");
  Serial.print(humidity);
  Serial.print("%");
  
  endlog();
  return;
}

int loopIndex = 0;
void loop() {
  delay(1);  
  
  loopIndex++;
  if (loopIndex>2000)
    loopIndex = 0;
    
  if (loopIndex == 2000)  {
    printdata();
    return;    
  }
    
  if (loopIndex % 1000 == 0) {
    readtemp();  
    return;
  }  
  
  if (loopIndex % 10 == 1)  {
    bool heatChanged;
    float temp = (temp_dallas1 + temp_dallas2)/2;
    mainlogic(temp, heatChanged);
    if (heatChanged) {
      printdata();
    }
    
    return;
  }
}

