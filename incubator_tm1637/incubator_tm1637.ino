#include "OneWire.h"
#include "EEPROM.h"
#include "DallasTemperature.h"
#include "TM1637Display.h"
#include "MegathTimer.h"
#include "MegathButton.h"
#include "MegathHeatRegulator.h"
#include "MegathTM1637InvertedHelper.h"
#include <avr/wdt.h>
#include <limits.h>

//===пины
#define button_pin 6
#define heat_relay_pin 11
#define motor_relay_pin 12
#define blink_pin 13
#define lcd1_clk_pin 5
#define lcd1_dio_pin 4
#define lcd2_clk_pin 3
#define lcd2_dio_pin 2
#define dallas_sgn_pin 8
#define alarm_pwr_pin A0

//==интервалы
#define heat_frame_duration 5000
#define tuning_big_step_timeout 1200
#define tuning_mode_enter_timeout 2000
#define tuning_mode_exit_timeout 8000

//===настройки отслеживания температуры (в градусах * 100)
//температура уставки по умолчанию
#define default_desiredtemp 3750
//мин. температура уставки
#define desiredtemp_min 3500
//макс. температура уставки
#define desiredtemp_max 4500

//==========параметры ПИД
//какая мощность будет по умолчанию (если в памяти некорректное значение)
#define default_power_ratio 0.35d
//Скорость регулировки. Применяется при корректировке мощности. Подбирается опытным путем
#define prop_ratio 0.015d
//задержка между грубой и тонкой подстройкой (если Tcurrent < Tset - rude_delta то включена грубая регулировка, т.е. мощность тупо на 100%)
#define rude_delta 1000
//максимальный уровень мощности, который можно держать неограниченно долгое время, не опасаясь расплавления оборудования
//или минимальный уровень мощности, при котором начинается опасность расплавления - смотря с какой точки зрения подойти :)
#define minimum_dangerous_power_ratio 0.5d
//продолжительность периода замера температуры. Регулирует баланс между точностью и скоростью настройки
#define regulator_range_duration 120l * 1000l
//сколько секунд можно держать максимальную мощность не боясь перегрева
#define full_power_max_seconds 4 * 60

MegathHeatRegulator regulator(prop_ratio, rude_delta);

//настройки сирены
#define beep_volume 130

//кнопки
#define button_pressed_value 0
MegathButton button(button_pin, button_pressed_value);

//настройки исполнительных механизмов
#define use_motor
//#define motor_dont_start_immediately

//для работы с датчиками
OneWire oneWire(dallas_sgn_pin);
DallasTemperature sensors(&oneWire);

//для дисплеев
TM1637Display display1(lcd1_clk_pin, lcd1_dio_pin);
TM1637Display display2(lcd2_clk_pin, lcd2_dio_pin);

//основные показатели
float humidity;
float temp_dallas1; //температура инкубатора

//таймеры
MegathTimer requesttempTimer;
MegathTimer printdataTimer;
MegathTimer regularDisplayTimer;
MegathTimer tuningDisplayTimer;
MegathTimer eggsTimer;
MegathTimer blinkTimer;
MegathTimer saveTimer;

//желаемая температура
int desiredtemp;
//верхняя граница при которой включать сирену (в градусах)
float panic_high;
//нижняя граница при которой включать сирену (в градусах)
float panic_low;

//последнее сохраненное значение мощности
double current_output;
//минимальный шаг мощности при сохранении
#define outputSaveMinDelta 0.1d

//режим изменения desiredtemp
bool tuningMode;
long tuningModeOffTime;

//вспомогательные буферы
uint8_t data4[4];
char cbuffer[50];

void setup()
{
  wdt_disable();

  //output
  Serial.begin(9600);
  Serial.println("start...");

  //load state
  loadState();

  regulator.SetPreciseOutput(current_output);
  applyDesiredTemp();

  // lcd
  data4[0] = 0xFF;
  data4[1] = 0xFF;
  data4[2] = 0xFF;
  data4[3] = 0xFF;

  display1.setBrightness(0x0f);
  display1.setSegments(data4);
  display2.setBrightness(0x0f);
  display2.setSegments(data4);

  delay(100);

  //dallas sensors
  sensors.begin();
  Serial.print("temperature sensors count: ");
  Serial.println(sensors.getDeviceCount());  
  delay(100);

  //relay
  pinMode(motor_relay_pin, OUTPUT);
  digitalWrite(motor_relay_pin, LOW);
  pinMode(heat_relay_pin, OUTPUT);
  digitalWrite(heat_relay_pin, LOW);

  //button
  pinMode(button_pin, INPUT_PULLUP);
  button.PressedInterval = tuning_big_step_timeout;
  button.LongPressInterval = tuning_mode_enter_timeout;
  button.ShortPressCallback = onButtonShortPress;
  button.LongPressCallback = onButtonLongPress;
  button.PressedCallback = onButtonPressed;

  //timers
  initTimers();

  //pid
  regulator.SetOutputLimits(0, heat_frame_duration);
  regulator.SetHeatProtectionParams(1, minimum_dangerous_power_ratio, full_power_max_seconds);
  regulator.SetRangeDuration(regulator_range_duration);

  //anti-freeze actions
  pinMode(blink_pin, OUTPUT);
  wdt_enable(WDTO_8S);

  //start signal
  pinMode(alarm_pwr_pin, OUTPUT);
  analogWrite(alarm_pwr_pin, 0);
  pinMode(alarm_pwr_pin, INPUT);
  beep();
}

void loadState()
{
  readFromEEPROM(&current_output, 0, sizeof(double));
  Serial.print("output loaded: ");
  Serial.println(current_output);
  if (current_output <= 0 || current_output > 1 || current_output != current_output)
  {
    current_output = default_power_ratio;
    Serial.println("output resetted to default");
  }
  Serial.print("final output value: ");
  Serial.println(current_output);

  readFromEEPROM(&desiredtemp, sizeof(double), sizeof(int));
  Serial.print("desiredtemp loaded: ");
  Serial.println(desiredtemp);
  if (desiredtemp < desiredtemp_min || desiredtemp > desiredtemp_max || desiredtemp != desiredtemp)
  {
    Serial.println("desiredtemp resetted to default");
    desiredtemp = default_desiredtemp;
  }
  Serial.print("final desiredtemp value: ");
  Serial.println(desiredtemp);
}

void readFromEEPROM(void *dest, int start, int length)
{
  for (int i = 0; i < length; i++)
  {
    ((byte *)dest)[i] = EEPROM.read(start + i);
  }
}

void writeToEEPROM(void *src, int start, int length)
{
  for (int i = 0; i < length; i++)
  {
    EEPROM.write(start + i, ((byte *)src)[i]);
  }
}

bool blinkOn = 0;
void onBlinkTimer()
{
  blinkOn = !blinkOn;
  if (blinkOn)
    digitalWrite(blink_pin, HIGH);
  else
    digitalWrite(blink_pin, LOW);
}

void onSaveTimer()
{
  double preciseOutput = regulator.GetPreciseOutput();

  Serial.print("save timer: current_output=");
  Serial.print(current_output);
  Serial.print("preciseOutput=");
  Serial.println(preciseOutput);
  
  if (fabs(current_output - preciseOutput) > outputSaveMinDelta)
  {
    writeToEEPROM(&preciseOutput, 0, sizeof(double));
    
    Serial.print("output saved, value = ");
    Serial.print(preciseOutput);
    Serial.print(", current_output = ");
    Serial.println(current_output);

    current_output = preciseOutput;
  }
  else
  {
    Serial.print("output not saved, value = ");
    Serial.print(preciseOutput);
    Serial.print(", current_output = ");
    Serial.println(current_output);
  }
}

long eggs_motor_off_time = 0;
void startEggsMotorIfNeeded()
{
  eggs_motor_off_time = millis() + 5 * 1000;

#ifdef use_motor
  Serial.println("motor on");
  digitalWrite(motor_relay_pin, HIGH);
#endif
}

void stopEggsMotorIfNeeded()
{
  if (eggs_motor_off_time > 0 && (millis() > eggs_motor_off_time || tuningMode))
  {
    eggs_motor_off_time = 0;
    Serial.println("motor off");
    digitalWrite(motor_relay_pin, LOW);
  }
}

bool is_temperature_requested;
void requestTemp() {
  sensors.requestTemperatures();
  is_temperature_requested = 1;
}

bool sensors_ignored;
void readTempIfNeeded() {
  if (is_temperature_requested) {
    float td1 = sensors.getTempCByIndex(0);
    if (td1 < -30)
    {
      if (!sensors_ignored)
      {
        Serial.print("sensors are ignored. sensor1 = ");
        Serial.print(td1);
        sensors_ignored = true;
      }
    }
    else
    {
      temp_dallas1 = td1;
  
      if (sensors_ignored)
      {
        Serial.println("sensors are restored");
        sensors_ignored = false;
      }
    }
    is_temperature_requested = 0;
  }
}

void printData()
{
  long currentMillis = millis();
  long hours = currentMillis / (1000l*60*60);
  currentMillis -= hours * 1000l*60*60;
  long minutes = currentMillis / (1000l*60);
  currentMillis -= minutes * 1000l*60;
  long seconds = currentMillis / 1000l;
  long mseconds = currentMillis % 1000l;

  sprintf(cbuffer, "%02ld:%02ld:%02ld:%03ld", hours, minutes, seconds, mseconds);
  Serial.print(cbuffer);
  Serial.print("\tt1=");
  Serial.print(temp_dallas1);  

  /*Serial.print("\thum=");
    Serial.print(humidity);
    Serial.print("%");*/

  Serial.print("\tinpt=");
  Serial.print(regulator.Input);

  Serial.print("\tsetpt=");
  Serial.print(regulator.Setpoint);

  Serial.print("\tpwr=");
  sprintf(cbuffer, "%04d/%d", regulator.Output, heat_frame_duration);
  Serial.print(cbuffer);

  Serial.println("");
}

void fillDisplayTemperature(int n, TM1637Display display)
{
  if (n > 9999)
    n = 9999;
  if (n < 0)
    n = 0;
  for (int i = 0; i < 4; i++)
  {
    int digit = n % 10;
    data4[i] = GetInvertedDigit(digit);
    n /= 10;
  }
  display.setSegments(data4);
}

void fillDisplayPower(int n, TM1637Display display)
{
  if (n > 999)
    n = 999;
  if (n < 0)
    n = 0;
  for (int i = 1; i < 4; i++)
  {
    int digit = n % 10;
    data4[i] = GetInvertedDigit(digit);
    n /= 10;
  }
  data4[0] = 0b00100100;
  display.setSegments(data4);
}

void fillDisplayBytes(byte b1, byte b2, byte b3, byte b4, TM1637Display display)
{
  data4[3] = b1;
  data4[2] = b2;
  data4[1] = b3;
  data4[0] = b4;

  display.setSegments(data4);
}

byte current_display_count = 0;
void updateRegularDisplay()
{
  switch (current_display_count % 3)
  {
  case 0:
    fillDisplayBytes(0b00111000, 0b00001000, 0, GetInvertedDigit(1), display1); //t 1
    fillDisplayTemperature(temp_dallas1 * 100, display2);
    break;
  case 1:
    fillDisplayBytes(0b01101101, 0b01001111, 0b00111000, 0b00001000, display1); //set
    fillDisplayTemperature(desiredtemp, display2);
    break;
  case 2:
    fillDisplayBytes(0b01011110, 0b00111111, 0b00100111, 0b00110011, display1); //pow
    fillDisplayPower(regulator.Output * 100l / heat_frame_duration, display2);
    break;
  }

  current_display_count++;  
}

void applyDesiredTemp()
{
  regulator.Setpoint = desiredtemp;
  regulator.CriticalInput = desiredtemp + 70;
  panic_high = desiredtemp + 150;
  panic_low = desiredtemp - 300;
}

uint8_t update_tuning_display_index = 0;
void updateTuningDisplay()
{
  if (tuningMode)
  {
    if (update_tuning_display_index % 4 == 1)
    {
      fillDisplayBytes(0, 0, 0, 0, display1);
    }
    else
    {
      fillDisplayBytes(0b01101101, 0b01001111, 0b00111000, 0b00001000, display1); //set
    }
    fillDisplayTemperature(desiredtemp, display2);

    update_tuning_display_index++;
  }
}

bool onButtonLongPress()
{
  Serial.println("longPress");
  if (!tuningMode)
  {
    startTuning();
    return true;
  }
  else
  {
    return false;
  }
}

void updateTuningModeOffTime()
{
  tuningModeOffTime = millis() + tuning_mode_exit_timeout;
}

void checkDesiredTemp()
{
  if (desiredtemp > desiredtemp_max || desiredtemp < desiredtemp_min)
  {
    desiredtemp = desiredtemp_min;
  }
  else
  {
    desiredtemp = (desiredtemp / 10) * 10;
  }
}

void onButtonShortPress()
{
  Serial.println("shortPress");
  if (tuningMode)
  {
    desiredtemp += 10;
    updateTuningModeOffTime();
    checkDesiredTemp();
  }
}

void onButtonPressed()
{
  Serial.println("pressed");
  if (tuningMode)
  {
    desiredtemp += 100;
    updateTuningModeOffTime();
    checkDesiredTemp();
  }
}

void beep()
{
  Serial.println("beep!");
  
  pinMode(alarm_pwr_pin, OUTPUT);
  analogWrite(alarm_pwr_pin, beep_volume);
  delay(2);
  analogWrite(alarm_pwr_pin, 0);
  pinMode(alarm_pwr_pin, INPUT);    
}

void startTuning()
{
  tuningMode = 1;
  updateTuningModeOffTime();
  updateHeatState();
  stopEggsMotorIfNeeded();
  beep();
}

void exitFromTuningIfNeeded()
{
  if (tuningModeOffTime < millis() && tuningMode == 1)
  {
    Serial.println("exitFromTuning");
    tuningMode = 0;
    applyDesiredTemp();
    updateRegularDisplay();    
    writeToEEPROM(&desiredtemp, sizeof(double), sizeof(int));
    beep();
  }  
}

void initTimers()
{
#ifdef motor_dont_start_immediately
  eggsTimer.start(startEggsMotorIfNeeded, 0);
#else
  eggsTimer.start(startEggsMotorIfNeeded, 1);
#endif
  eggsTimer.intervalMS = 60 * 60 * 1000l;
  //eggsTimer.debug = 1;
  
  requesttempTimer.start(requestTemp, 1);
  requesttempTimer.intervalMS = 1000l;
  printdataTimer.start(printData, 0);
  printdataTimer.intervalMS = 5000l;
  regularDisplayTimer.start(updateRegularDisplay, 1);
  regularDisplayTimer.intervalMS = 1500l;
  tuningDisplayTimer.start(updateTuningDisplay, 1);
  tuningDisplayTimer.intervalMS = 100l;
  blinkTimer.start(onBlinkTimer, 1);
  blinkTimer.intervalMS = 500l;
  saveTimer.start(onSaveTimer, 0);
  saveTimer.intervalMS = 60 * 60 * 1000l;
}

void updateHeatState() {
  if (tuningMode) {
    digitalWrite(heat_relay_pin, LOW);
  } else {
    long currentMillis = millis();    

    long position = currentMillis % heat_frame_duration;
    
    if (position <= regulator.Output && !sensors_ignored) {
      digitalWrite(heat_relay_pin, HIGH);
    } else {      
      digitalWrite(heat_relay_pin, LOW);
    }
  }
}

void loop()
{
  wdt_reset();

  button.UpdateInput();

  if (tuningMode) 
  {
     tuningDisplayTimer.update();
     exitFromTuningIfNeeded();
  } else
  {
    eggsTimer.update();
    requesttempTimer.update();
    printdataTimer.update();
    regularDisplayTimer.update();    
    blinkTimer.update();    
  
    button.UpdateInput();

    if (!sensors_ignored) {
      saveTimer.update();
      regulator.Input = temp_dallas1 * 100;
      regulator.update();
    }
  
    exitFromTuningIfNeeded();
    stopEggsMotorIfNeeded();
    readTempIfNeeded();
    updateHeatState();
  }

  wdt_reset();
}
