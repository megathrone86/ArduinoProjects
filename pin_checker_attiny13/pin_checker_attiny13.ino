//  +-\/-+
// 5|o   |V
// 3|    |2
// 4|    |1
// G|    |0
//  +----+

void writeToRelay(uint8_t value) {
  digitalWrite(0, value);
  digitalWrite(1, value);
  digitalWrite(2, value);
  digitalWrite(3, value);
}

void setup() {
  for (int i = 0; i++; i < 5) {
    pinMode(i, OUTPUT);
    digitalWrite(i, LOW);
  }
}

void loop() {
  writeToRelay(HIGH);
  //digitalWrite(0, HIGH);
  //digitalWrite(1, HIGH);
  delay(100);
  writeToRelay(LOW);
  //digitalWrite(0, LOW);
  //digitalWrite(1, LOW);
  delay(100);
}
