#include "OneWire.h"
#include "EEPROMex.h"
#include "DallasTemperature.h"
#include "MegathTimer.h"
#include "MegathHeatRegulator.h"
#include <avr/wdt.h>
#include <LiquidCrystal.h>
#include "Megath5StateButton.h"

//настройки отслеживания температуры
//температура уставки по умолчанию (в градусах)
#define default_desired_temp 375
//температура при которой обогрев надо отключать полюбому
#define criticaltemp (default_desired_temp + 50)

//параметры ПИД
#define default_power_ratio 0.35d
//какая мощность будет по умолчанию (если в памяти некорректное значение)
#define prop_ratio 0.95d
//сколько единиц мощности (в нашем случае 1 единица = 1 промилле) соответствуют одной единице измерения Input (в нашем случае 1 единица = 1 градус Цельсия). Применяется при корректировке мощности
#define default_rude_delta 45
//задержка по умолчанию между грубой и тонкой подстройкой (если Tcurrent < Tset - rude_delta то включена грубая регулировка, т.е. мощность тупо на 100%)
#define dangerous_power_threshold 0.5d
//максимальный уровень мощности, который можно держать неограниченно долгое время, не опасаясь расплавления оборудования
//или минимальный уровень мощности, при котором начинается опасность расплавления - смотря с какой точки зрения подойти :)
#define regulator_range_duration 90l*1000l
//продолжительность периода замера температуры. Регулирует баланс между точностью и скоростью настройки
#define default_maxFullPower 4
//сколько секунд можно держать максимальную мощность не боясь перегрева
#define WindowSize 1000
//комнатная температура (экономим на датчиках, да)
#define temp_outside 240
unsigned long windowStartTime;
MegathHeatRegulator regulator(prop_ratio, default_rude_delta);

//настройки датчиков температуры
#define dallas_temp_precision 9
#define dallas1_sgn 11

//настройки исполнительных механизмов
#define heat_relay_pin 2
#define motor_relay_pin 3
#define use_motor
//#define motor_dont_start_immediately
#define blink_pin 13

//EEPROM
#define address_base 0
#define address_preciseOutput address_base+10
#define address_desiredtemp address_base+20
#define address_swapOutputs address_base+30
#define address_turnTime address_base+40
#define address_maxFullPower address_base+50

//для работы с датчиками
OneWire oneWire1(dallas1_sgn);
DallasTemperature sensor1(&oneWire1);

//LCD
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
#define buttons_pin A0

//основные показатели
//short humidity;
int temp_inside; //температура инкубатора
int desiredtemp; //желаемая температура инкубатора
bool swapOutputs;
int maxFullPower;

//таймеры
MegathTimer eggsTimer;
#define default_turn_time 60 * 60 * 1000L
MegathTimer saveTimer;
#define outputSaveMinDelta 0.1d

int GetHeatPin() {
  return swapOutputs ? motor_relay_pin :  heat_relay_pin;
}

int GetMotorPin() {
  return swapOutputs ? heat_relay_pin :  motor_relay_pin;
}

void setup() {
  wdt_disable();

  // lcd
  lcd.begin(16, 2);
  pinMode(buttons_pin, INPUT);

  delay(500);

  //anti-freeze actions
  pinMode(blink_pin, OUTPUT);
  wdt_enable(WDTO_8S);

  //output
  Serial.begin(9600);

  //dallas sensors
  sensor1.begin();
  delay(500);

  //relay
  pinMode(motor_relay_pin, OUTPUT);
  digitalWrite(motor_relay_pin, LOW);
  pinMode(heat_relay_pin, OUTPUT);
  digitalWrite(heat_relay_pin, LOW);

  //timers
#ifdef motor_dont_start_immediately
  eggsTimer.start(on_eggs_start, 0);
#else
  eggsTimer.start(on_eggs_start, 1);
#endif
  saveTimer.start(on_save, 0);
  saveTimer.intervalMS = (long) 60 * 60 * 1000;

  //pid
  windowStartTime = millis();
  regulator.SetOutputLimits(0, WindowSize);
  regulator.SetHeatProtectionParams(1, dangerous_power_threshold, maxFullPower * 60);
  regulator.SetRangeDuration(regulator_range_duration);
  regulator.CriticalInput = criticaltemp;

  load_params();

  for (int i = 0; i < 5; i++) {
    delay(100);
    readtemp();
  }
}

void load_params() {
  double output = EEPROM.readDouble(address_preciseOutput);
  if (output <= 0 || output > 1 || isnan(output)) {
    output = default_power_ratio;
  }
  regulator.SetPreciseOutput(output);

  desiredtemp = EEPROM.readInt(address_desiredtemp);
  if (desiredtemp < 250 || desiredtemp > 600 || isnan(desiredtemp)) {
    desiredtemp = default_desired_temp;
  }

  swapOutputs = EEPROM.readByte(address_swapOutputs);

  eggsTimer.intervalMS = EEPROM.readLong(address_turnTime);
  if (eggsTimer.intervalMS < 30 * 60 * 1000L || eggsTimer.intervalMS > 4 * 60 * 60 * 1000L || isnan(eggsTimer.intervalMS))
    eggsTimer.intervalMS = default_turn_time;

  maxFullPower = EEPROM.readInt(address_maxFullPower);
  if (maxFullPower < 1 || maxFullPower > 60)
    maxFullPower = default_maxFullPower;
  regulator.SetHeatProtectionParams(1, dangerous_power_threshold, maxFullPower * 60);
}

byte blinkOn = 0;
void on_blink() {
  blinkOn++;
  digitalWrite(blink_pin, blinkOn % 4 == 0 ? HIGH : LOW);
}

void on_save() {
  double lastSavedOutput = EEPROM.readDouble(address_preciseOutput);
  double preciseOutput = regulator.GetPreciseOutput();

  if (fabs(lastSavedOutput - preciseOutput) > outputSaveMinDelta) {
    EEPROM.updateDouble(address_preciseOutput, preciseOutput);
  }
}

unsigned long eggs_motor_off_time = 4294967295;
void on_eggs_start() {
  eggs_motor_off_time = millis() + 5 * 1000;

#ifdef use_motor
  digitalWrite(GetMotorPin(), HIGH);
#endif
}

void process_eggs() {
  if (millis() > eggs_motor_off_time) {
    eggs_motor_off_time = 4294967295;
    digitalWrite(GetMotorPin(), LOW);
  }
}

void readtemp() {
  sensor1.requestTemperatures();
  short td1 = sensor1.getTempCByIndex(0) * 10;
  if (td1 < -400)
    td1 = -400;
  if (td1 > 990)
    td1 = 990;
  temp_inside = td1;
}


bool updateDisplay;
char buf[30], buf2[8];

void append_int(int n) {
  itoa(n, buf2, 10);
  strcat(buf, buf2);
}
void append_char(char c) {
  buf2[0] = c;
  buf2[1] = 0;
  strcat(buf, buf2);
}
void append_string(char* c) {
  strcat(buf, c);
}

void append_pseudo_float(int n) {
  append_int(n / 10);
  append_char('.');
  append_int(n % 10);
}
void append_temp(int n) {
  if (n <= -400 || n >= 990)
    append_string("N/A");
  else
    append_pseudo_float(n);
}
void lcdprint(char *string) {
  if (!updateDisplay)
    return;
  strcpy(buf, string);
  for (int i = strlen(string); i < 16; i++) {
    buf[i] = ' ';
  }
  buf[16] = 0;
  lcd.print(buf);
}

#define keyDelay 500L
long keyReleaseTime;

void MarkKeyPressed() {
  keyReleaseTime = millis() + keyDelay;
}

//0 - текущее состояние
//1 - настройка температуры
byte mode;
void processUI() {
  lcd.setCursor(0, 0);
  byte key = PressedButtonIndex(analogRead(A0));
  if (keyReleaseTime > millis())
    key = 0;
  if (key == 4)
    mode = 0;

  switch (mode) {
    case 0:
      buf[0] = 0;
      append_string("c:");
      append_temp(temp_inside);
      append_string("  pwr:");
      append_int(regulator.Output * 100 / WindowSize);
      append_char('%');
      lcdprint(buf);

      lcd.setCursor(0, 1);

      buf[0] = 0;
      append_string("s:");
      append_temp(desiredtemp);
      append_string("  trn:");
      append_int(((long)eggsTimer.msecs_to_event()) / 1000 / 60);
      append_char('m');
      lcdprint(buf);

      if (key == 5) {
        mode = 1;
        MarkKeyPressed();
      }
      break;
    case 1:
      buf[0] = 0;
      lcdprint("set desired temp");
      lcd.setCursor(0, 1);
      buf[0] = 0;
      append_temp(desiredtemp);
      lcdprint(buf);

      switch (key) {
        case 2:
          if (desiredtemp < 500)
            desiredtemp += 1;
          MarkKeyPressed();
          break;
        case 3:
          if (desiredtemp > 200)
            desiredtemp -= 1;
          MarkKeyPressed();
          break;
        case 5:
          EEPROM.updateInt(address_desiredtemp, desiredtemp);
          mode = 2;
          MarkKeyPressed();
          break;
      }
      break;
    case 2:
      lcdprint("swap outputs");
      lcd.setCursor(0, 1);
      if (swapOutputs)
        lcdprint("heat:1 turn:2");
      else
        lcdprint("heat:2 turn:1");

      switch (key) {
        case 2:
        case 3:
          swapOutputs = !swapOutputs;
          MarkKeyPressed();
          break;
        case 5:
          EEPROM.updateByte(address_swapOutputs, swapOutputs);
          mode = 3;
          MarkKeyPressed();
          break;
      }
      break;
    case 3:
      {
        lcdprint("manual power set");
        lcd.setCursor(0, 1);

        double preciseOutput = regulator.GetPreciseOutput();
        buf[0] = 0;
        append_int(100 * preciseOutput);
        append_char('%');
        lcdprint(buf);

        switch (key) {
          case 2:
            if (preciseOutput < 0.99d)
              regulator.SetPreciseOutput(preciseOutput + 0.01d);
            MarkKeyPressed();
            break;
          case 3:
            if (preciseOutput > 0.01d)
              regulator.SetPreciseOutput(preciseOutput - 0.01d);
            MarkKeyPressed();
            break;
          case 5:
            EEPROM.updateDouble(address_preciseOutput, regulator.GetPreciseOutput());
            mode = 4;
            MarkKeyPressed();
            break;
        }
      }
      break;
    case 4:
      {
        lcdprint("turn time set");
        lcd.setCursor(0, 1);

        long interval = ((long)eggsTimer.intervalMS) / 1000 / 60;
        buf[0] = 0;
        append_int(interval);
        append_string(" minutes");
        lcdprint(buf);

        switch (key) {
          case 2:
            if (interval < 4 * 60 - 5)
              eggsTimer.intervalMS = (interval + 5) * 60 * 1000;
            MarkKeyPressed();
            break;
          case 3:
            if (interval > 35)
              eggsTimer.intervalMS = (interval - 5) * 60 * 1000;
            MarkKeyPressed();
            break;
          case 5:
            EEPROM.updateLong(address_turnTime, eggsTimer.intervalMS);
            mode = 5;
            MarkKeyPressed();
            break;
        }
      }
      break;
    case 5:
      {
        lcdprint("max full power");
        lcd.setCursor(0, 1);

        buf[0] = 0;
        append_int(maxFullPower);
        append_string(" minutes");
        lcdprint(buf);

        switch (key) {
          case 2:
            if (maxFullPower < 60)
              maxFullPower++;
            MarkKeyPressed();
            break;
          case 3:
            if (maxFullPower > 1)
              maxFullPower--;
            MarkKeyPressed();
            break;
          case 5:
            EEPROM.updateInt(address_maxFullPower, maxFullPower);
            regulator.SetHeatProtectionParams(1, dangerous_power_threshold, maxFullPower * 60);
            mode = 0;
            MarkKeyPressed();
            break;
        }
      }
      break;
  }
}

short loopIndex = 0;
void loop() {
  eggsTimer.update();
  saveTimer.update();

  if (loopIndex % 3000 == 0)
    readtemp();
  updateDisplay = (loopIndex % 100 == 0) ? 1 : 0;
  processUI();
  if (loopIndex % 500 == 0)
    on_blink();

  process_eggs();

  regulator.Input = temp_inside - temp_outside;
  regulator.Setpoint = desiredtemp - temp_outside;
  regulator.update();
  if (millis() > windowStartTime + WindowSize)
    windowStartTime = millis();
  digitalWrite(GetHeatPin(), (millis() < windowStartTime + regulator.Output) ? HIGH : LOW);

  if (loopIndex > 3000)
    loopIndex = 0;
  else
    loopIndex++;
  wdt_reset();
}
