#include <LiquidCrystal.h>
//#include "MegathButton.h";

//LiquidCrystal lcd(4, 5, 6, 7, 8, 9);
LiquidCrystal lcd(8, 9, 4, 5, 6, 7);

//MegathButton btns(A0, 5);

void setup() {
  Serial.begin(9600);
  Serial.println("start...");

  lcd.begin(16, 2);

  pinMode(A0, INPUT);
}

void loop() {
  int r = analogRead(A0);
  Serial.println(r);
  delay(1000);

  lcd.clear();
  lcd.print("Analog input:");
  lcd.setCursor(0, 1);
  lcd.print(r);
}
