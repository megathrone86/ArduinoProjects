#ifndef MegathHeatRegulator_H
#define MegathHeatRegulator_H

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#define MegathHeatRegulator_RANGE_START_UNDEFINED -1
#define MegathHeatRegulator_RANGE_DURATION 180l * 1000l

class MegathHeatRegulator
{
private:
	//настройки
	double PropRatio; //сколько процентов мощности соответствуют одному градусу?
	int outputMin;
	int RudeDelta;
	int rangeStartInput;
	bool directionIsUp;
	long rangeDuration = MegathHeatRegulator_RANGE_DURATION;

	//защита от перегрева
	bool heatProtectionEnabled = 0;
	double heatProtectionMinimumDangerousPowerRatio;
	int heatProtectionFullPowerMaxSeconds;
	long heatProtectionCounter = 0;
	long resetHeatProtectionCounterTime = 0;

	//расчеты
	int outputRange;
	double outputRatio;
	long rangeStart;
	bool rudeMode;
	long lastUpdateTime;

	void OnRangeEnded();
	void OnStabilized();
	void UpdateOutput();
	void StartNewRange();
	void OnRudeModeChanged();
	bool IsRude();
	double GetActualOutputRatio();

public:
	//настройки
	int Input, Setpoint, Output, CriticalInput;
	MegathHeatRegulator(double propRatio, int rudeDelta);
	void SetOutputLimits(int min, int max);
	void update();

	double GetPreciseOutput();
	void SetPreciseOutput(double value);
	void SetHeatProtectionParams(bool enabled, double minimumDangerousPowerRatio, int fullPowerMaxSeconds);
	long GetHeatProtectionCounter();
	void SetRangeDuration(long value);
};

#endif