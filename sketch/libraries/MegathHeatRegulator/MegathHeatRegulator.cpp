#include "MegathHeatRegulator.h"

MegathHeatRegulator::MegathHeatRegulator(double propRatio, int rudeDelta) {
	PropRatio = propRatio;
	rangeStart = MegathHeatRegulator_RANGE_START_UNDEFINED;
	outputRatio = 0.0d;
	RudeDelta = rudeDelta;
	lastUpdateTime = millis();
}

void MegathHeatRegulator::SetOutputLimits(int min, int max) {
	outputMin = min;
	outputRange = max - min;
	
	UpdateOutput();
}

void MegathHeatRegulator::OnRudeModeChanged() {	

#ifdef MegathHeatRegulator_Debug
	Serial.print("regulator: OnRudeModeChanged to ");
	Serial.println(rudeMode);
#endif

	if (rudeMode) {		
		UpdateOutput();
	} else {
		rangeStart = MegathHeatRegulator_RANGE_START_UNDEFINED;
		UpdateOutput();
	}
}

void MegathHeatRegulator::update() {
	bool newRudeMode = IsRude();
	if (rudeMode != newRudeMode) {
		rudeMode = newRudeMode;
		OnRudeModeChanged();
	}
	
	if (!rudeMode) {
		if (rangeStart == MegathHeatRegulator_RANGE_START_UNDEFINED)
			StartNewRange();
		
		if (rangeStart + rangeDuration < millis()) {
			OnRangeEnded();
		}
	}
	
	if (heatProtectionEnabled) {
		long delta = millis() - lastUpdateTime;		
		lastUpdateTime = millis();
		double actualRatio = GetActualOutputRatio();			
		
		//full power = 1, zero power = -1, heatProtectionMinimumDangerousPowerRatio - zero point
		double ratio = 0;
		if (actualRatio > heatProtectionMinimumDangerousPowerRatio)
			ratio = (actualRatio - heatProtectionMinimumDangerousPowerRatio) / (1.0d - heatProtectionMinimumDangerousPowerRatio); 
		if (actualRatio < heatProtectionMinimumDangerousPowerRatio)
			ratio = (actualRatio - heatProtectionMinimumDangerousPowerRatio) / heatProtectionMinimumDangerousPowerRatio;		
		
		heatProtectionCounter += (long)((double)(ratio * delta) / 1000);
		if (heatProtectionCounter < 0)
			heatProtectionCounter = 0;
		if (heatProtectionCounter > heatProtectionFullPowerMaxSeconds && resetHeatProtectionCounterTime <= 0) {

#ifdef MegathHeatRegulator_Debug
			Serial.println(
			"heatProtectionCounter reached critical value. Power will be restricted to heatProtectionMinimumDangerousPowerRatio at the next heatProtectionFullPowerMaxSeconds interval");
#endif
			resetHeatProtectionCounterTime = millis() + heatProtectionFullPowerMaxSeconds * 1000;
			UpdateOutput();
		}
	}
	
	if (resetHeatProtectionCounterTime > 0 && millis() > resetHeatProtectionCounterTime) {
		
#ifdef MegathHeatRegulator_Debug
		Serial.println("heatProtectionCounter was reset to 0");
#endif

		heatProtectionCounter = 0;
		resetHeatProtectionCounterTime = 0;
		UpdateOutput();
	}
}

bool MegathHeatRegulator::IsRude() {
	return (Input + RudeDelta) < Setpoint;
}

void MegathHeatRegulator::OnRangeEnded() {
	int delta = Input - rangeStartInput;
	
#ifdef MegathHeatRegulator_Debug
	Serial.print("regulator: OnRangeEnded ");
	Serial.print("delta=");
	Serial.print(Input);
	Serial.print("-");
	Serial.print(rangeStartInput);
	Serial.print("=");
	Serial.print(delta);
	Serial.print(", up=");
	Serial.print(directionIsUp);
#endif
	
	bool stabilized;
	if (directionIsUp)
		stabilized = delta <= 0;
	else
		stabilized = delta >= 0;
	
#ifdef MegathHeatRegulator_Debug
	Serial.print(", stabilized=");
	Serial.println(stabilized);
#endif
	
	if (stabilized || Input >= CriticalInput) {
		OnStabilized();
	}

	StartNewRange();
}

void MegathHeatRegulator::OnStabilized() {

#ifdef MegathHeatRegulator_Debug
	Serial.println("regulator: OnStabilized");
#endif
	
	double delta = Setpoint - Input;
	outputRatio += delta * PropRatio / 100.0d;
	if (outputRatio <= 0)
		outputRatio = 0;
	else if (outputRatio > 1)
		outputRatio = 1;
	
	UpdateOutput();
}

double MegathHeatRegulator::GetActualOutputRatio() {		
	double result = 1;
	if (!rudeMode)
		result = outputRatio;
	
	if (heatProtectionCounter > heatProtectionFullPowerMaxSeconds && result > heatProtectionMinimumDangerousPowerRatio)
		return heatProtectionMinimumDangerousPowerRatio;
	else
		return result;
}

void MegathHeatRegulator::UpdateOutput() {
	double ratio = GetActualOutputRatio();
	Output = outputMin + outputRange * ratio;

#ifdef MegathHeatRegulator_Debug	
	Serial.print("regulator: UpdateOutput ");
	if (heatProtectionCounter > heatProtectionFullPowerMaxSeconds) {
		Serial.print("[restricted]");
	}
	Serial.println(Output);
#endif	
}

void MegathHeatRegulator::StartNewRange() {
    rangeStart = millis();
	directionIsUp = Input < Setpoint;
	rangeStartInput = Input;

#ifdef MegathHeatRegulator_Debug		
	Serial.print("regulator: StartNewRange up=");
	Serial.println(directionIsUp);
#endif
}

double MegathHeatRegulator::GetPreciseOutput() {
	return outputRatio;
}

void MegathHeatRegulator::SetPreciseOutput(double value) {
	outputRatio = value;
	
	UpdateOutput();
}

void MegathHeatRegulator::SetHeatProtectionParams(bool enabled, double minimumDangerousPowerRatio, int fullPowerMaxSeconds) {
	heatProtectionEnabled = enabled;
	heatProtectionFullPowerMaxSeconds = fullPowerMaxSeconds;
	heatProtectionMinimumDangerousPowerRatio = minimumDangerousPowerRatio;
	
	heatProtectionCounter = 0;
}

long MegathHeatRegulator::GetHeatProtectionCounter() {
	return heatProtectionCounter;
}

void MegathHeatRegulator::SetRangeDuration(long value) {
	rangeDuration = value;
}