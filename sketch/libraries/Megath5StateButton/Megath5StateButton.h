#ifndef Megath5StateButton_H
#define Megath5StateButton_H

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

//0 140 320 500 740 1024

//		140
// 500 			0	740
//		320

//		2
// 4		1	5
//		3

byte PressedButtonIndex(short int rawInput);

#endif