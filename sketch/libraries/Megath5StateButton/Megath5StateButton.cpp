#include "Megath5StateButton.h"

//0 140 320 500 740 1024

//		140
// 500 			0	740
//		320

//		2
// 4		1	5
//		3

byte PressedButtonIndex(short int rawInput) {
	if (rawInput < 100)
		return 1;
	if (rawInput < 200)
		return 2;
	if (rawInput < 400)
		return 3;
	if (rawInput < 600)
		return 4;
	if (rawInput < 900)
		return 5;
	return 0;
}