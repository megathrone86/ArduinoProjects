#ifndef MegathTimer_H
#define MegathTimer_H

//#define MegathTimer_Debug

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

class MegathTimer {
 private:
  long counter;
  long lastUpdate;
  void (*callback)(void);
  
 public:  
#ifdef MegathTimer_Debug
  bool debug;
#endif
  long intervalMS;
  
  MegathTimer();
  void update();
  void start(void (*pcallback)(void), bool immediate);
  long msecs_to_event();
};

#endif