#include "MegathTimer.h"

MegathTimer::MegathTimer() {	
}

void MegathTimer::update() {
	counter += millis() - lastUpdate;
	bool b = counter > intervalMS;
	lastUpdate = millis();

#ifdef MegathTimer_Debug
	if (debug) {
		Serial.println("MegathTimer::update()");
		Serial.print(intervalMS);
		Serial.print(" ");
		Serial.print((long)millis());
		Serial.print(" ");
		Serial.print(counter);		
		Serial.print(" ");
		Serial.println(b);
	}
#endif
	
	if (intervalMS>0 && b) {
		if (callback)
			callback();
		counter = 0;
	}
}

void MegathTimer::start(void (*pcallback)(void), bool immediate) {
	callback = pcallback;
	intervalMS = 1000*60;
	
	counter = 0;
	lastUpdate = millis();
	
	if (immediate)
		callback();
}

long MegathTimer::msecs_to_event() {
	return intervalMS - counter;
}