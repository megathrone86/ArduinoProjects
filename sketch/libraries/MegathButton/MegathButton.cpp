#include "MegathButton.h"

MegathButton::MegathButton(uint8_t pin, uint8_t pressed_value)
{
	this->pin = pin;
	this->pressed_value = pressed_value;
}

void MegathButton::UpdateInput()
{
	bool pressed = digitalRead(pin) == pressed_value;
	long currentMillis = millis();
	if (pressed)
	{
		if (!last_input)
		{
			if ((currentMillis - last_down_time) >= AntiBounceInterval)
			{
				last_input = 1;
				last_down_time = currentMillis;
				last_pressed_start_time = currentMillis;

				if (DownCallback)
				{
					DownCallback();
				}
			}
		}
		else
		{
			if (currentMillis - last_pressed_start_time >= PressedInterval && !prevent_pressed_events)
			{
				if (PressedCallback)
				{
					PressedCallback();
				}
				last_pressed_start_time = currentMillis;
			}

			if ((currentMillis - last_down_time) > LongPressInterval && !long_pressed)
			{
				if (LongPressCallback)
				{
					prevent_pressed_events = LongPressCallback();
				}
				long_pressed = 1;
			}
		}
	}
	else
	{
		if (last_input)
		{
			if (!long_pressed)
			{
				if (ShortPressCallback)
				{
					ShortPressCallback();
				}
			}

			long_pressed = 0;
			prevent_pressed_events = 0;
			if (UpCallback)
			{
				UpCallback();
			}
		}
	}
	last_input = pressed;
}