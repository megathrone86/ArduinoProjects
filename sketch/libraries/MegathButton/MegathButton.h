#ifndef MegathButton_H
#define MegathButton_H

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

class MegathButton
{
private:
  //settings
  uint8_t pin;
  uint8_t pressed_value;

  //vars
  uint8_t last_input;
  long last_down_time;
  long last_pressed_start_time;
  bool long_pressed;
  bool prevent_pressed_events;

public:
  MegathButton(uint8_t pin, uint8_t pressed_value);
  void UpdateInput();

  long PressedInterval = 250;
  long LongPressInterval = 2000;
  long AntiBounceInterval = 100;

  //should return true if need to prevent pressed events
  bool (*LongPressCallback)(void);

  void (*ShortPressCallback)(void);
  void (*DownCallback)(void);
  void (*UpCallback)(void);
  void (*PressedCallback)(void);
};

#endif