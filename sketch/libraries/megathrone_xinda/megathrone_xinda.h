#ifndef megathrone_xinda_H
#define megathrone_xinda_H

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

class megathrone_xinda {
 private:
  uint8_t pin;
  uint16_t zeroValue;
  float rawValueToDegreesRatio;

 public:
  megathrone_xinda(uint8_t pin);
  void init();
  void calibrate(float degrees1, uint16_t pinData1, float degrees2, uint16_t pinData2);
  int readRawData();
  float readTemperature();
};

#endif