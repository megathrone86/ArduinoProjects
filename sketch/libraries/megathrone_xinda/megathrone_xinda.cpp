#include "megathrone_xinda.h"

megathrone_xinda::megathrone_xinda(uint8_t pin) {
	this->pin = pin;
	
	pinMode(pin, INPUT);
}

void megathrone_xinda::init() {
	pinMode(pin, INPUT);
}

//										x1					y1				x2				y2
void megathrone_xinda::calibrate(float degrees1, uint16_t pinData1, float degrees2, uint16_t pinData2) {
	zeroValue = (pinData1 * degrees2 - pinData2 * degrees1) / (degrees2 - degrees1);
	/*Serial.print("zeroValue = ");
	Serial.print(zeroValue);
	Serial.print("\t");*/
	
	rawValueToDegreesRatio = (int)(pinData2 - pinData1) / (degrees2 - degrees1);
	/*Serial.print("rawValueToDegreesRatio = ");
	Serial.print(rawValueToDegreesRatio);
	Serial.println();*/
}

int megathrone_xinda::readRawData() {
	return analogRead(pin);
}

float megathrone_xinda::readTemperature() {
	int t = readRawData();
	return (t - (int)zeroValue) / rawValueToDegreesRatio;
}