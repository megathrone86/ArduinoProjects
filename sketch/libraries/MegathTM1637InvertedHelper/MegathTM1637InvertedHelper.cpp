#include "MegathTM1637InvertedHelper.h"

// direct 7-segment digit scheme (for TM1637)
//  --7--
//  2   6
//  --1--
//  3   5
//  --4--

// inverted 7-segment digit scheme (for TM1637)
//  --4--
//  5   3
//  --1--
//  6   2
//  --7--

//01234567

uint8_t invertedDigits[] = {
      0b00111111,    // 0
      0b00110000,    // 1
      0b01011011,    // 2
      0b01111001,    // 3
      0b01110100,    // 4
      0b01101101,    // 5
      0b01101111,    // 6
      0b00111000,    // 7
      0b01111111,    // 8
      0b01111101,    // 9      
    };

uint8_t GetInvertedDigit(uint8_t digit) {
  if (digit < 0 || digit > 9) 
    return 0;
  return invertedDigits[digit];
}