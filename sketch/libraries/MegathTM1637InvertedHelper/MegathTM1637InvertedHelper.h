#ifndef MegathTM1637InvertedHelper_H
#define MegathTM1637InvertedHelper_H

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

uint8_t GetInvertedDigit(uint8_t digit);

#endif