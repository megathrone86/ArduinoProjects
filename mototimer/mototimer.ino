//#define EXTENDED_CONSOLE_OUTPUT

#define DEFAULT_DETECT_DISTANCE 150
#define MIN_DETECT_DISTANCE 50
#define MAX_DETECT_DISTANCE 200
#define DETECT_DISTANCE_CHANGE_STEP 5
#define DETECT_DISTANCE_SHOW_TIME 2000
// #define DETECT_DISTANCE_SHOW_TIME 5000
//частота измерений
#define MEASURE_FREQUENCY 100
//#define MEASURE_FREQUENCY 1
//частота обновления дисплея
#define DISPLAY_UPDATE_FREQUENCY 5
//через какое время девайс сбрасывает состояние (переходит из режима FINISH (отображение результатов) в IDLE (ожидание начала нового заезда))
#define IDLE_RESET_TIME_MS 5000
//для смены режима нужно чтобы показания не менялись на протяжении указанного интервала
#define MODE_CHANGE_STABILITY_SERIES_LENGTH 4
//минимальное время круга (до этого времени финиш не засчитывается
// #define MIN_LAP_TIME 10000
#define MIN_LAP_TIME 5000

#define SENSOR_TRIG_PIN 4
#define SENSOR_ECHO_PIN 2
#define BUTTON_PIN 3
#define LCD_ADDRESS 0x3F

#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include "MegathTimer.h"
#include "MegathButton.h"
LiquidCrystal_I2C lcd(LCD_ADDRESS, 16, 2); // A4, A5

MegathButton button(BUTTON_PIN, LOW);

unsigned long displayMillis;

bool blinkOn = false;
unsigned long blinkMillis = 0;

MegathTimer measureTimer, displayTimer;

//если датчик показывает расстояние меньше этого значения, считаем что спортсмен в зоне действия датчика
volatile int detectDistance = DEFAULT_DETECT_DISTANCE;
volatile unsigned long detectDistanceEditStartMillis;
volatile int distance;
volatile bool isObstacleDetected;
volatile unsigned long startMeasureMicros;
volatile bool isMeasuring;

#define MODE_IDLE 0
#define MODE_ATTEMPTING_START 1
#define MODE_STARTED 2
#define MODE_ATTEMPTING_FINISH 3
#define MODE_FINISHED 4
volatile byte mode;

volatile unsigned long lap_start_time;
volatile unsigned long lap_end_time;
volatile unsigned long last_lap_time = 0;
unsigned long prev_lap_time = 0;

byte series[MODE_CHANGE_STABILITY_SERIES_LENGTH];
unsigned int series_index;

void setMode(byte newMode) {
#ifdef EXTENDED_CONSOLE_OUTPUT
  Serial.print("new mode: ");
  Serial.println(newMode);
#endif
  mode = newMode;
}

void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);

  pinMode(SENSOR_TRIG_PIN, OUTPUT);
  pinMode(SENSOR_ECHO_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(SENSOR_ECHO_PIN), onTriggerPinChanged, CHANGE);

  pinMode(BUTTON_PIN, INPUT_PULLUP);
  digitalWrite(1, BUTTON_PIN);
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), onButtonPinChanged, CHANGE);
  button.PressedCallback = onButtonPress;
  button.DownCallback = onButtonPress;
  button.PressedInterval = 500;
  correctDetectDistance();

  measureTimer.start(startMeasure, 0);
  measureTimer.intervalMS = 1000L / MEASURE_FREQUENCY;
  displayTimer.start(updateDisplay, 0);
  displayTimer.intervalMS = 1000L / DISPLAY_UPDATE_FREQUENCY;

  lcd.init();
  lcd.backlight();

  lcd.clear();
  lcd.setCursor(4, 0);
  lcd.print("test");
  delay(100);
  lcd.clear();
  Serial.println("");
  Serial.println("system started");
}

void startMeasure()
{
  if (isMeasuring) return;
  digitalWrite(SENSOR_TRIG_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(SENSOR_TRIG_PIN, HIGH);
  delayMicroseconds(10);
  isMeasuring = 1;
  digitalWrite(SENSOR_TRIG_PIN, LOW);
  //startMeasureMicros = micros();
}
void onTriggerPinChanged() {
  if (digitalRead(SENSOR_ECHO_PIN) == HIGH) {
    startMeasureMicros = micros();
  }
  else if (isMeasuring) {
    onMeasureCompleted();
  }
}
void onButtonPinChanged() {
  button.UpdateInput();
  //Serial.print("button ");
  //Serial.println(digitalRead(BUTTON_PIN));
}
void onButtonPress() {
  detectDistance += DETECT_DISTANCE_CHANGE_STEP;
  correctDetectDistance();
  detectDistanceEditStartMillis = millis();
}
void correctDetectDistance() {
  if (detectDistance < MIN_DETECT_DISTANCE) {
    detectDistance = MIN_DETECT_DISTANCE;
  } else if (detectDistance > MAX_DETECT_DISTANCE) {
    detectDistance = MIN_DETECT_DISTANCE;
  }
}
bool getObstacleDetected() {
  return distance < detectDistance;
}
void onMeasureCompleted() {
  unsigned long duration = micros() - startMeasureMicros;
  distance = duration * 0.034 / 2;
  isMeasuring = 0;

  isObstacleDetected = getObstacleDetected();

  series_index++;
  series[series_index % MODE_CHANGE_STABILITY_SERIES_LENGTH] = isObstacleDetected;

  bool isStable = true;
  for (int i = 1; i < MODE_CHANGE_STABILITY_SERIES_LENGTH; i++) {
    if (series[i] != isObstacleDetected) {
      isStable = false;
      break;
    }
  }

  if (!isStable)
    return;

#ifdef EXTENDED_CONSOLE_OUTPUT
  //Serial.print(" duration: ");
  //Serial.print(duration);
  //Serial.print(" distance: ");
  //Serial.print(distance);
  //Serial.print(" detectDistance: ");
  //Serial.print(detectDistance);
  //Serial.print(" isObstacleDetected: ");
  //Serial.print(isObstacleDetected);
  //Serial.println();
#endif

  switch (mode) {
    case MODE_IDLE:
      if (isObstacleDetected) {
#ifdef EXTENDED_CONSOLE_OUTPUT
        Serial.print(" distance: ");
        Serial.print(distance);
        Serial.print(" isObstacleDetected: ");
        Serial.print(isObstacleDetected);
        Serial.println();
#endif

        lap_start_time = millis();
        setMode(MODE_ATTEMPTING_START);
      }
      break;
    case MODE_ATTEMPTING_START:
      if (!isObstacleDetected) {
        lap_start_time = millis();
        setMode(MODE_STARTED);
      }
      break;
    case MODE_STARTED:
      if (isObstacleDetected) {
#ifdef EXTENDED_CONSOLE_OUTPUT
        Serial.print(" distance: ");
        Serial.print(distance);
        Serial.print(" isObstacleDetected: ");
        Serial.print(isObstacleDetected);
        Serial.println();
#endif
        unsigned long lap_time = millis() - lap_start_time;
        if (lap_time < MIN_LAP_TIME)
          break;

        lap_end_time = millis();
        last_lap_time = lap_end_time - lap_start_time;
        setMode(MODE_ATTEMPTING_FINISH);
      }
      break;
    case MODE_ATTEMPTING_FINISH:
      if (!isObstacleDetected) {
        setMode(MODE_FINISHED);

#ifdef EXTENDED_CONSOLE_OUTPUT
        unsigned long t = last_lap_time;
        unsigned long minutes = t / (1000L * 60);
        t -= minutes * 1000L * 60;
        unsigned long seconds = t / 1000L;
        t -= seconds * 1000L;
        unsigned long milliseconds = t;

        Serial.print("lap finished: ");
        Serial.print(last_lap_time);
        Serial.print(" (");
        Serial.print(minutes);
        Serial.print(":");
        Serial.print(seconds);
        Serial.print(":");
        Serial.print(milliseconds);
        Serial.println(")");
#endif
      }
      break;
    case MODE_FINISHED:
      if (!isObstacleDetected && ((millis() - lap_end_time) > IDLE_RESET_TIME_MS)) {
        prev_lap_time = last_lap_time;
        setMode(MODE_IDLE);
      }
      break;
  }
}

void loop() {
  unsigned long currentMillis = millis();

  //blink
  if (currentMillis - blinkMillis > 800) {
    blinkOn = !blinkOn;
    digitalWrite(LED_BUILTIN, blinkOn);
    blinkMillis = currentMillis;
  }

  //start measure if needed
  measureTimer.update();
  displayTimer.update();

  button.UpdateInput();
}

char tline[20], tline2[20];

void printTime(unsigned long current_time, char *dst) {
  unsigned long t = current_time;
  unsigned long minutes = t / (1000L * 60);
  t -= minutes * 1000L * 60;
  unsigned long seconds = t / 1000L;
  t -= seconds * 1000L;
  unsigned long milliseconds = t;

  sprintf(dst, "%02d:%02d:%03d", (word)minutes, (word)seconds, (word)milliseconds);
}

unsigned long getCurrentTimeForDisplay() {
  switch (mode) {
    case MODE_STARTED:
      return millis() - lap_start_time;
    case MODE_ATTEMPTING_FINISH:
    case MODE_FINISHED:
      return lap_end_time - lap_start_time;
    default:
      return 0;
  }
}

byte idle_mode_blink_counter;

void updateDisplay() {
  if ((millis() - detectDistanceEditStartMillis) < DETECT_DISTANCE_SHOW_TIME) {
    char cdistance[4];
    sprintf(cdistance, "%03d", detectDistance);
    sprintf(tline, "DETECTION    %s", cdistance);
    sprintf(tline2, "                ");
  } else {
    char ctime[10];
    if (mode == MODE_IDLE && (idle_mode_blink_counter++) % 6 > 3) {
      sprintf(ctime, "%s", "         ");
    } else {
      unsigned long current_time = getCurrentTimeForDisplay();
      printTime(current_time, ctime);
    }

    char cdistance[4];
    if (distance > 400) {
      sprintf(cdistance, "MAX");
    } else if (distance <= 0) {
      sprintf(cdistance, "%s", "ERR");
    } else {
      sprintf(cdistance, "%03d", distance);
    }

    sprintf(tline, "%s    %s", ctime, cdistance);

    printTime(prev_lap_time, ctime);
    if (isObstacleDetected) {
      sprintf(tline2, "%s detect", ctime);
    } else {
      sprintf(tline2, "%s       ", ctime);
    }
  }

  //lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(tline);
  lcd.setCursor(0, 1);
  lcd.print(tline2);
}
