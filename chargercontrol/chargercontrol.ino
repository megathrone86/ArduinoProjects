#define minChargeVoltageF 12.5f //пороговое напряжение. если напряжение на аккуме ниже чем пороговое, то начинаем новый зарядки
#define chargePeriodSeconds 5*60 //длина одного цикла зарядки в секундах (по истечении этого периода переходим в режим измерения напряжения на аккуме)
#define analogInPin 5
#define voltageDividerRatio 4 //коэффициент делителя напряжения (если максимально нужно измерять 20 вольт, а на вход можно подавать максимум 5 вольт, то надо делить на 4)
#define minChargeVoltage (1024 * minChargeVoltageF / voltageDividerRatio / 5) //преобразованное пороговое напряжение, для того чтобы уйти от операций с float и сэкономить память МК
#define relayPin 0

void setup() {
  pinMode(analogInPin, INPUT);
  pinMode(relayPin, OUTPUT);
}

int chargingSecondsLeft = 0;
void loop() {

  //какой сейчас режим?
  if (chargingSecondsLeft <= 0) {
    //измерение напряжения на аккуме
    digitalWrite(relayPin, LOW);

    //измерим напряжение
    int sensorValue = analogRead(analogInPin);

    //если оно ниже минимально допустимого
    if (sensorValue < minChargeVoltage)
      //то начнем цикл заряда
      chargingSecondsLeft = chargePeriodSeconds;

    delay(1000);
  } else {
    //идет цикл заряда, отсчитываем секунды до завершения
    digitalWrite(relayPin, HIGH);
    delay(1000);
    chargingSecondsLeft--;
  }
}
