#include "MegathButton.h";

int pin = 9;

void setup() {
  Serial.begin(9600);
  Serial.println("start...");

  pinMode(pin, OUTPUT);
}

void loop() {
  digitalWrite(pin, HIGH);
  delay(1000);
  digitalWrite(pin, LOW);
  delay(100);
}
