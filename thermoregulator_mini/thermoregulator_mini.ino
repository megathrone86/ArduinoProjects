#include "EEPROM.h"
#include "DHT.h"
#include "TM1637Display.h"
#include "MegathTimer.h"

//lcd
#define LCD_CLK 5
#define LCD_DIO 4
TM1637Display display(LCD_CLK, LCD_DIO);
uint8_t displayData[4];

//sensors
#define sensor_sgn 2
DHT dht(sensor_sgn, DHT11);

//relay
#define relay_sgn 3

//buttons
#define LONG_BUTTON_DELAY 800L
#define button_out A1

//timers
MegathTimer readtempTimer;
MegathTimer updateDisplayTimer;
MegathTimer saveTimer;

//logic
#define MIN_RELAY_ON_INTERVAL 10*60*1000

//vars
float currentTemp;
float setTemp = 5;

//timing
#define SetpointViewDuration 2500

void setup() {
  Serial.begin(9600);
  Serial.println("start...");

  int addr = 100;
  byte* ptr = (byte*) &setTemp;
  *ptr = EEPROM.read(addr);
  addr++; ptr++;
  *ptr = EEPROM.read(addr);
  addr++; ptr++;
  *ptr = EEPROM.read(addr);
  addr++; ptr++;
  *ptr = EEPROM.read(addr);

  printTimeStamp();
  Serial.print("loaded setpoint: ");
  Serial.println(setTemp);

  if (setTemp < 0)
    setTemp = 0;
  if (setTemp > 99)
    setTemp = 99;
  if (isnan(setTemp))
    setTemp = 5;

  //sensors
  pinMode(sensor_sgn, INPUT);
  dht.begin();

  // lcd
  displayData[0] = 0xFF;
  displayData[1] = 0xFF;
  displayData[2] = 0xFF;
  displayData[3] = 0xFF;

  display.setSegments(displayData);
  display.setBrightness(0xff);
  delay(SetpointViewDuration);

  //relay
  pinMode(relay_sgn, OUTPUT);
  digitalWrite(relay_sgn, LOW);

  //button
  pinMode(button_out, INPUT);

  //таймеры
  readtempTimer.start(on_readtemp, 1000l, 1);
  updateDisplayTimer.start(showStates, 100l, 1);
  saveTimer.start(on_save, 10 * 1000l, 1);
}

void loop() {
  readtempTimer.update();
  updateDisplayTimer.update();
  saveTimer.update();

  processButton();

  processUIStates();
  processLogic();

  delay(1);
}

int relayState = 0;
long lastRelayOn = 0;
void processLogic() {
  if (currentTemp < setTemp) {
    if ((millis() - lastRelayOn) >= MIN_RELAY_ON_INTERVAL) {
      changeRelayState(HIGH);
      lastRelayOn = millis();

      printTimeStamp();
      Serial.println("lastRelayOn updated");
    }
  }
  else
    changeRelayState(LOW);
}

void printTimeStamp() {
  Serial.print(millis() / 1000);
  Serial.print(".");
  Serial.print(millis() % 1000);
  Serial.print("\t ");
}

void changeRelayState(int newState) {
  if (relayState != newState) {
    relayState = newState;
    digitalWrite(relay_sgn, relayState);

    printTimeStamp();
    Serial.print("relayState is changed to ");
    Serial.print(relayState);
    Serial.println("");
  }
}

void fillDisplayTemperature(char c, int n, TM1637Display display) {
  if (n > 9999)
    n = 9999;
  if (n < 0)
    n = 0;

  displayData[0] = 0b00000000;
  displayData[1] = 0b00000000;
  if (c == 's') {
    displayData[0] = 0b01101110;
    displayData[1] = 0b00001000;
  }

  for (int i = 3; i >= 2; i--)
  {
    int digit = n % 10;
    displayData[i] = display.encodeDigit(digit);
    n /= 10;
  }
}

bool save_active;
void on_save() {
  if (!save_active)
    return;

  int addr = 100;
  byte* ptr = (byte*) &setTemp;
  EEPROM.write(addr, *ptr);
  addr++; ptr++;
  EEPROM.write(addr, *ptr);
  addr++; ptr++;
  EEPROM.write(addr, *ptr);
  addr++; ptr++;
  EEPROM.write(addr, *ptr);

  printTimeStamp();
  Serial.print("saved setpoint: ");
  Serial.println(setTemp);

  save_active = 0;
}

void on_readtemp() {
  currentTemp = dht.readTemperature();

  printTimeStamp();
  Serial.print("currentTemp = ");
  Serial.print(currentTemp);
  Serial.println("");
}

bool is_button_pressed() {
  int rawData = analogRead(button_out);

  bool btState = rawData > 800;
  digitalWrite(13, btState);
  return btState;
}

enum currentStateEnum { Default, SetpointView, SetpointSet1, SetpointSet2 };
currentStateEnum currentState = Default;
long lastStateChangeTime = 0;

void UpdateLastStateChangeTime() {
  lastStateChangeTime = millis();
}
void on_button_up(bool longPressed) {
  switch (currentState) {
    case Default:
      if (longPressed) {
        currentState = SetpointSet1;
        UpdateLastStateChangeTime();
      }
      else {
        currentState = SetpointView;
        UpdateLastStateChangeTime();
      }
      break;
    case SetpointView:
      currentState = Default;
      UpdateLastStateChangeTime();
      break;
    case SetpointSet1:
      if (longPressed) {
        currentState = SetpointSet2;
        UpdateLastStateChangeTime();
      }
      else {
        setTemp += 10;
        if (setTemp / 10 >= 10)
          setTemp -= 100;
        UpdateLastStateChangeTime();
        save_active = 1;
      }
      break;
    case SetpointSet2:
      if (longPressed) {
        currentState = Default;
        UpdateLastStateChangeTime();
      }
      else {
        setTemp++;
        if ((int)setTemp % 10 == 0)
          setTemp -= 10;
        UpdateLastStateChangeTime();
        save_active = 1;
      }
      break;
  }
}

#define SetpointViewAutoOffTime 1000
#define SetpointSetAutoOffTime 15 * 1000

void processUIStates() {
  switch (currentState) {
    case SetpointView:
      if ((millis() - lastStateChangeTime) > SetpointViewAutoOffTime) {
        currentState = Default;
        UpdateLastStateChangeTime();
      }
      break;
    case SetpointSet1:
    case SetpointSet2:
      if ((millis() - lastStateChangeTime) > SetpointSetAutoOffTime) {
        currentState = Default;
        UpdateLastStateChangeTime();
      }
      break;
  }
}

#define blinkTime 400l
long blinkStartTime;
bool blinkState;

void showStates() {
  switch (currentState) {
    case Default:
      fillDisplayTemperature('c', currentTemp, display);
      display.setSegments(displayData);
      break;
    case SetpointView:
      fillDisplayTemperature('s', setTemp, display);
      display.setSegments(displayData);
      break;
    case SetpointSet1:
      fillDisplayTemperature('s', setTemp, display);

      if ((millis() - blinkStartTime) > blinkTime) {
        blinkState = !blinkState;
        blinkStartTime = millis();
      }
      if (!blinkState)
        displayData[2] = 0;
      display.setSegments(displayData);

      break;
    case SetpointSet2:
      fillDisplayTemperature('s', setTemp, display);

      if ((millis() - blinkStartTime) > blinkTime) {
        blinkState = !blinkState;
        blinkStartTime = millis();
      }
      if (!blinkState)
        displayData[3] = 0;
      display.setSegments(displayData);

      break;
  }
}

bool btn_down = 0;
bool btn_lock = 0;
long btn_down_time = 0;
long btn_off_start_time = -1;
#define KeyPressMinInterval 600
#define KeyOffFilterTime 30
long lastKeyUpTime;

void processButton() {
  bool pressed = is_button_pressed();
  if (!pressed && (btn_down || btn_lock)) {
    if (btn_off_start_time == -1)
      btn_off_start_time = millis();

    if (millis() - btn_off_start_time < KeyOffFilterTime)
      pressed = 1;
    else
      btn_off_start_time = -1;
  }

  if (!pressed && btn_lock)
    btn_lock = 0;

  if (pressed && !btn_lock) {
    if (!btn_down && ((millis() - lastKeyUpTime) > KeyPressMinInterval)) {
      btn_down = 1;
      btn_down_time = millis();
    }
  }

  bool longpress_over = btn_down && ((millis() - btn_down_time) > LONG_BUTTON_DELAY);
  bool shortpress_over = btn_down && !pressed;
  if (longpress_over) {
    btn_down = 0;
    btn_lock = 1;
    on_button_up(true);
    lastKeyUpTime = millis();
  }
  if (shortpress_over) {
    btn_down = 0;
    on_button_up(false);
    lastKeyUpTime = millis();
  }

  digitalWrite(13, pressed);
}

